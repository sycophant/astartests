#################
#A*
#################

import heapq  #imports priority queue class

class PriorityQueue:  #local implementation of priority queue

    def __init__(self):
        self.elements = []
    
    def empty(self):
        return len(self.elements) == 0
    
    def put(self, item, priority):
        heapq.heappush(self.elements, (priority, item))
    
    def get(self):
        return heapq.heappop(self.elements)[1]

def pathMemory(pathHistory, start, end):

    temp = end
    path = []

    while temp != start:
        path.append(temp)
        temp = pathHistory[temp]

    return path

def heuristic(a, b):  #heuristic takes x,y coordinates
    
    (x1, y1) = a  #break up coordinates
    (x2, y2) = b

    crowDist = abs(x1 - x2) + abs(y1 - y2)  #calculate euclidean cost

    return crowDist

def aStar(graph, start, end): #takes python graph, starting and ending point

    frontier = PriorityQueue()
    frontier.put(start, 0)  #initializes priority queue

    pathHistory = {}
    pathCost = {}
    pathHistory[start] = None
    pathCost[start] = 0 #initializes path history and cost with first values
    
    while not frontier.empty(): #expands frontier for optimal path

        current = frontier.get()
        if current == end:  #when optimal path's been found
            break
        
        for next in graph.neighbors(current):
            tempCost = pathCost[current] + graph.cost(current, next)  #create possible new path cost
            if next not in pathCost or tempCost < pathCost[next]: #push nodes to frontier and path history
                pathCost[next] = tempCost
                priority = tempCost + heuristic(end, next)  #apprend crowDist to new node
                frontier.put(next, priority)
                pathHistory[next] = current
    
    return pathHistory, pathCost  #return the route and cost of route