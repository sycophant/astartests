from aStar import *
from gridDraw import *

##################
##GRID GENERATE
##################

start, stop = (1, 1), (18, 18)

pathHistory, pathCost = aStar(graph, start, stop)

drawGrid(graph, width=3, number=pathCost, path=pathMemory(pathHistory, start=start, end=stop))
print()

pathHistory2, pathCost2 = aStar(graphForest, start, stop)

drawGrid(graphForest, width=3, number=pathCost2, path=pathMemory(pathHistory2, start=start, end=stop))
print()

pathHistory3, pathCost3 = aStar(graphDesert, start, stop)

drawGrid(graphDesert, width=3, number=pathCost3, path=pathMemory(pathHistory3, start=start, end=stop))