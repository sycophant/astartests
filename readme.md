# A* Approaches

Showcases two approaches to the A* algorithm. We initially considered A* to be entirely AI-driven, ergo we created an agent to drive itself using crow/manhattan distance through a generated map a la roguelikes and find the shortest path. We were unable to implement random map generation, ergo used a couple of pre-set maps, as shown below.

![](Demos/pythonout.png)

Second approach is player-driven. The player defines the start and the end points, and given that the algorithm is fed the worldspace through an array of cells or a navmesh, it will generate the shortest path via crow distance, as shown below.

![](Demos/unreal.gif)